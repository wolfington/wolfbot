import asyncio

from discord.ext import commands


class Admin(commands.Cog):

    def __init__(self, client):
        self.client = client

    # clears chat depending on given amount of messages
    @commands.command(pass_context=True)
    @commands.has_permissions(administrator=True)
    async def clear(self, ctx, amount: int):
        """
            Clears the last x number of posts
        """
        channel = ctx.message.channel
        messages = []
        if amount < 1:
            message = await ctx.send("You can't delete less than one message")
            await asyncio.sleep(2)
            await message.delete()
        else:
            async for message in channel.history(limit=amount + 1):
                messages.append(message)
            await channel.delete_messages(messages)

    @commands.command(pass_context=True, aliases=['m'])
    async def mute(self, ctx):
        if ctx.author.voice and ctx.author.voice.channel:
            channel = ctx.author.voice.channel
            for member in channel.members:
                await member.edit(mute=True)
        else:
            await ctx.send("You are not connected to a voice channel")

    @commands.command(pass_context=True, aliases=['um'])
    # @commands.has_permissions(administrator=True)
    async def unmute(self, ctx):
        if ctx.author.voice and ctx.author.voice.channel:
            channel = ctx.author.voice.channel
            for member in channel.members:
                await member.edit(mute=False)
        else:
            await ctx.send("You are not connected to a voic"
                           "e channel")

    @commands.command(pass_context=True, aliases=['sm'])
    @commands.has_permissions(administrator=True)
    async def solomute(self, ctx):
        if ctx.author.voice and ctx.author.voice.channel:
            channel = ctx.author.voice.channel
            for member in channel.members:
                await member.edit(mute=True)
            await ctx.author.edit(mute=False)
        else:
            await ctx.send("You are not connected to a voic"
                           "e channel")


def setup(client):
    client.add_cog(Admin(client))
