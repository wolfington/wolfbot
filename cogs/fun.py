import random
import asyncio
from datetime import datetime

import discord
from discord.ext import commands


class Fun(commands.Cog):

    def __init__(self, client):
        self.client = client

    # event within a cog always uses this decorator
    # self must always the first parameter for every function in cog
    @commands.Cog.listener()
    async def on_ready(self):
        print(datetime.now())
        print('Bot is online')

    # Commands

    fish_sent = [":fish: **{} nimmt einen Aal und schlägt {}.** :fishing_pole_and_fish:",
                 ":fish: **{} haut mit einer Flunder auf {} ein** :fishing_pole_and_fish:",
                 ":fish: **{} nimmt einen gümmeligen Dorsch und haut ihn {} auf den Kopp** :fishing_pole_and_fish:",
                 ":fish: **Die Scholle von {} trifft {} mitten ins Gesicht** :fishing_pole_and_fish:",
                 ":fish: **Die launische Forelle von {} überrascht {} komplett** :fishing_pole_and_fish:",
                 ":fish: **{} trägt einen Hecht bei sich, um {} damit zu schlagen** :fishing_pole_and_fish:",
                 ":fish: **{} hat einen Barsch in der Hand und wirf ihn auf {}** :fishing_pole_and_fish:",
                 ":fish: :fish: :fish: :fish: :fish: :fish: :fish: :fish: :fish: :fish: :fish: :fish: :fish: :fish:"]

    @commands.command()
    async def fishslap(self, ctx, other: discord.User):
        author = ctx.author
        channel = ctx.channel
        author_nick = channel.guild.get_member(author.id).nick
        if author_nick is None:
            author_nick = author.name
        other_nick = channel.guild.get_member(other.id).nick
        if other_nick is None:
            other_nick = other.name
        num = random.randrange(0, 5)
        await ctx.channel.send(self.fish_sent[num].format(author_nick, other_nick))

    @fishslap.error
    async def fishslap_error(self, ctx, error):
        if isinstance(error, commands.UserNotFound):
            await ctx.channel.send(
                "Benutzernamen nicht gefunden. Wenn du @ eingibst, "
                "kriegst du eine Liste mit allen verfügbaren Mitgliedern angezeigt")

    @commands.command(aliases=['münze'], brief=('Wirft eine Münze mit Kopf und Zahl'))
    async def coin(self, ctx):
        values = ['Zahl', 'Kopf']
        await ctx.channel.send(random.choice(values))

    @commands.command(aliases=['zufallszahl', 'zz', 'randomnumber'], brief='Zufällige Zahlen ermitteln')
    async def random(self, ctx, *numbers: int):

        if len(numbers) == 0:
            lower = 1
            upper = 100
        elif len(numbers) == 1:
            lower = 1
            upper = numbers[0]
        elif len(numbers) > 2:
            raise
        else:
            lower = min(numbers)
            upper = max(numbers)

        if lower == upper:
            choice = lower
        else:
            choice = random.choice(range(lower, upper + 1))

        await ctx.channel.send(choice)

    @random.error
    async def random_error(self, ctx, error):
        if isinstance(error, commands.BadArgument) or isinstance(error, commands.CommandInvokeError):
            await ctx.channel.send('''
            Bitte gib
- keine Zahl (ergibt eine Zufallszahl zwischen 1 und 100),
- eine Zahl x (ergibt eine Zufallszahl zwischen 1 und x) oder
- zwei Zahlen x y (ergibt eine Zufallszahl zwischen x und y)
hinter dem Befehl ein''')

    @commands.command(aliases=['zufallsauswahl', 'auswahl'])
    async def randomchoice(self, ctx, *options: str):
        message = '''
*Beep, beep*
:crystal_ball: *WolfBot denkt angestrengt nach und wählt aus:* :magic_wand:\n
'''
        await ctx.channel.send(message)
        await asyncio.sleep(0.5)
        await ctx.channel.send(":confetti_ball: **" +
                               random.choice(options) + "** :confetti_ball:")


def setup(client):
    client.add_cog(Fun(client))
