import discord
import re
from discord.ext import commands
from discord.ext.commands import BadArgument


class Poll(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.command(pass_context=True, aliases=["p"])
    async def poll(self, ctx, question, *options: str):

        # Check if length of options is between 2 and 20
        if len(options) <= 1:
            message_text = 'Bitte gib mindestens 2 Optionen an '
            await self.poll_wrong_arguments(ctx, message_text, "Less than 2 options.")
        elif len(options) > 20:
            message_text = "Du kannst nicht mehr als 20 Optionen angeben. Danke ~~Merkel~~ Discord"
            await self.poll_wrong_arguments(ctx, message_text, "More than 20 options")

        if len(options) == 2 \
                and options[0].lower() in ['ja', 'j', 'yes', 'y'] \
                and options[1].lower() in ['nein', 'no', 'n']:
            reactions = ['✅', '❌']
        else:
            first_emoji = 127462  # regional-indicator A
            reactions = [chr(first_emoji + x) for x in range(len(options))]

        # Description =  Emojis (regional_indicator) + initial values + options,
        description = '\n'.join(['{} **(0)** {}'.format(reactions[x], option) for x, option in enumerate(options)])

        # Build Embed
        embed = discord.Embed(title='Umfrage: ' + question, description=description,
                              color=discord.Colour.red())

        # delete the message the poll was created by
        await ctx.message.delete()

        # send message and react to it
        message = await ctx.send(embed=embed)
        for reaction_index in range(len(options)):
            await message.add_reaction(reactions[reaction_index])

        # set footer and add it to message (necessary to find message)
        embed.set_footer(text='{}'.format(message.id))
        await message.edit(embed=embed)

    async def poll_wrong_arguments(self, ctx, error_message, exception_message):
        message = await ctx.send(error_message)
        await message.delete(delay=5)
        await ctx.message.delete()
        raise BadArgument(exception_message)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        try:
            await self.update_poll(payload)
        except:
            return

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        try:
            await self.update_poll(payload)
        except:
            return

    async def update_poll(self, payload):
        channel = self.client.get_channel(payload.channel_id)
        message_id = payload.message_id
        message = await channel.fetch_message(message_id)
        try:
            embed = message.embeds[0]
            if int(embed.footer.text) == message_id:
                embed = await update_embed(embed, message)
                await message.edit(embed=embed)
        except:
            raise (Exception)

    @commands.command(pass_context=True, aliases=["e"])
    async def evaluate(self, ctx, message_id: int):

        channel = ctx.channel
        message = await channel.fetch_message(message_id)
        await ctx.message.delete()

        try:
            embed = message.embeds[0]
            title = embed.title.replace('Umfrage: ', 'Ergebnisse: ')

            unformatted_options = [x.strip() for x in embed.description.split('\n')]
            options = {opt[0]: re.search(r"(\s[a-zA-Z\d]+$)", opt).group().strip() for opt in unformatted_options}

            results = dict()
            for reaction in [rct for rct in message.reactions if rct.emoji in options]:
                temp_users = [channel.guild.get_member(user.id).nick or user.name async for user in reaction.users() if
                              not user.bot]
                results[options.get(reaction.emoji)] = (reaction.count, ", ".join(temp_users))

            result_string = ''
            for option in {key: results[key] for key in sorted(results, key=results.get, reverse=True)}:
                vote_count = results.get(option)[0] - 1
                if vote_count < 1:
                    continue
                voters = str(results.get(option)[1])
                result_string += "** {}: {} {} {})**\n".format(option,
                                                               str(vote_count),
                                                               " Stimme (" if vote_count == 1 else " Stimmen (",
                                                               voters)

            new_embed = discord.Embed(title=title,
                                      description=result_string,
                                      color=discord.Colour.green())
            await message.edit(embed=new_embed)
            for r in message.reactions:
                await message.clear_reaction(r)

        except Exception as e:
            print(e)
            return


def setup(bot):
    bot.add_cog(Poll(bot))


async def update_embed(embed, message):
    descr_options = [x.strip() for x in embed.description.split('\n')]
    options = {opt[0]: re.search(r"(\s[a-zA-Z\d]+$)", opt).group().strip() for opt in descr_options}

    description2 = [rct.emoji + ' **(' + str(rct.count - 1) + ')** ' + options.get(rct.emoji)
                    for rct in message.reactions if rct.emoji in options]

    embed.description = "\n".join(description2)
    return embed
