import discord
from discord.ext import commands
import re

import wikipedia


class Wiki(commands.Cog):
    wikipedia.set_lang("de")

    def __init__(self, client):
        self.client = client

    @commands.command(aliases=['wiki'],
                      brief='Informationen direkt von Wikipedia')
    async def wikipedia(self, ctx, interest: str):
        search_results = wikipedia.search(interest, results=10)

        if len(search_results) == 0:
            embed = discord.Embed(title=":triumph: Hrmpf. "
                                        "Ich konnte leider nichts finden. :triumph:")
        elif len(search_results) == 1:
            embed = await result_embed(ctx, self.client, search_results[0])
        else:
            title = "Das sind die besten {} Ergebnisse deiner Suche. " \
                    "Bitte such einen Artikel aus".format(len(search_results))
            article = await get_choice(ctx, self.client, search_results, title)
            embed = await result_embed(ctx, self.client, article)
        await ctx.send(embed=embed)


async def get_choice(ctx, client, search_results, title):
    search_results_dict = dict()
    first_emoji = 127462

    # limit to 5 results
    for x in range(len(search_results)):
        search_results_dict[chr(first_emoji + x)] = search_results[x]

    description = '\n'.join([f'{key}: {value}' for key, value in search_results_dict.items()])

    embed_search = discord.Embed(title=title, description=description)

    message = await ctx.send(embed=embed_search)
    for emoji in search_results_dict:
        await message.add_reaction(emoji)

    try:
        reaction, user = await client.wait_for('reaction_add',
                                               check=lambda reaction, user:
                                               user == ctx.author
                                               and str(reaction.emoji) in search_results_dict.keys(),
                                               timeout=20)
        emoji = reaction.emoji
        article_name = search_results_dict.get(emoji)
    except Exception:
        article_name = search_results[0]

    await message.delete()
    return article_name


async def result_embed(ctx, client, article_name):
    try:
        page = wikipedia.page(article_name, auto_suggest=False)
    except wikipedia.DisambiguationError as e:
        # TODO: let user choose the right option
        print(e.options)
        title = "Zu diesem Begriff gibt es mehrere Seiten: Welche meinst du genau?"
        article_name = await get_choice(ctx, client, e.options[:10], title)
        page = wikipedia.page(article_name, auto_suggest=False)
    # load summary of wikipedia article
    summary = page.summary

    # embed texts can only hold up to 2048 letters
    if len(summary) > 2000:
        summary_splits = re.split(r'[.;]\s', summary)
        summary = ''
        for part in summary_splits:
            if len(summary) + len(part) < 2042:
                summary += ' ' + part
            else:
                break
        summary += " ... "

    # create embed
    embed_result = discord.Embed(
        title=page.title,
        description=summary,
        color=discord.Colour.light_gray()
    )
    if page.images:
        embed_result.set_thumbnail(url=page.images[0])
    embed_result.add_field(name="Link", value=page.url, inline=False)

    return embed_result


def setup(client):
    client.add_cog(Wiki(client))
