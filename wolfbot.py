import os
from discord.ext import commands
import discord

from dotenv import load_dotenv
load_dotenv()
DISCORD_TOKEN = os.getenv("DISCORD_TOKEN")

intents = discord.Intents.default()
intents.members = True

client = commands.Bot(command_prefix="&", intents=intents)


@client.command(aliases=['load'])
@commands.has_permissions(administrator=True)
async def activate_extension(ctx, extension):
    client.load_extension(f'cogs.{extension}')


@client.command(aliases=['unload'])
@commands.has_permissions(administrator=True)
async def disable_extension(ctx, extension):
    client.unload_extension(f'cogs.{extension}')

for filename in os.listdir('./cogs'):
    if filename.endswith('.py'):
        client.load_extension(f'cogs.{filename[:-3]}')

client.run(DISCORD_TOKEN)
